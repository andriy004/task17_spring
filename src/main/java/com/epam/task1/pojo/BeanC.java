package com.epam.task1.pojo;

import com.epam.task1.MyBeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanC implements MyBeanValidator {

    private static Logger logger = LogManager.getLogger(BeanC.class);
    private String name;
    private int value;

    public BeanC(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void customInitMethod() {
        logger.debug("BeanC -> customInitMethod()");
    }

    public void customDestroyMethod() {
        logger.debug("BeanC -> customDestroyMethod()");
    }

    @Override
    public void validate() {
        logger.debug("BeanC -> validate()[interface MyBeanValidator]");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
