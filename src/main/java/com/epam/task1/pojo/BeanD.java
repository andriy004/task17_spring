package com.epam.task1.pojo;

import com.epam.task1.MyBeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanD implements MyBeanValidator {

    private static Logger logger = LogManager.getLogger(BeanD.class);
    private String name;
    private int value;

    public BeanD(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void customInitMethod() {
        logger.debug("BeanD -> customInitMethod()");
    }

    public void customDestroyMethod() {
        logger.debug("BeanD -> customDestroyMethod()");
    }

    @Override
    public void validate() {
        logger.debug("BeanD -> validate()[interface MyBeanValidator]");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
