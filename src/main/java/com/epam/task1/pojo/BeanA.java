package com.epam.task1.pojo;

import com.epam.task1.MyBeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements MyBeanValidator, InitializingBean, DisposableBean {

    private static Logger logger = LogManager.getLogger(BeanA.class);
    private String name;
    private int value;

    public void customInitMethod() {
        logger.debug("BeanA -> customInitMethod()");
    }

    public void customDestroyMethod() {
        logger.debug("BeanA -> customDestroyMethod()");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.debug("BeanA -> afterPropertiesSet()[interface InitializingBean]");
    }

    @Override
    public void destroy() throws Exception {
        logger.debug("BeanA -> destroy()[interface DisposableBean]");
    }

    @Override
    public void validate() {
        logger.debug("BeanA -> validate()[interface MyBeanValidator]");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
