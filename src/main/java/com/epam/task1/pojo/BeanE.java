package com.epam.task1.pojo;

import com.epam.task1.MyBeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements MyBeanValidator {

    private static Logger logger = LogManager.getLogger(BeanE.class);
    private String name;
    private int value;

    public void customInitMethod() {
        logger.debug("BeanE -> customInitMethod()");
    }

    public void customDestroyMethod() {
        logger.debug("BeanE -> customDestroyMethod()");
    }

    @Override
    public void validate() {
        logger.debug("BeanE -> validate()[interface MyBeanValidator]");
    }

    @PostConstruct
    public void posConstruct() {
        logger.debug("BeanE -> posConstruct()[annotation @PostConstruct]");
    }

    @PreDestroy
    public void PreDestroy() {
        logger.debug("BeanE -> PreDestroy()[annotation @PreDestroy]");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
