package com.epam.task1.pojo;

import com.epam.task1.MyBeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanF implements MyBeanValidator {

    private static Logger logger = LogManager.getLogger(BeanF.class);
    private String name;
    private int value;

    public void customInitMethod() {
        logger.debug("BeanF -> customInitMethod()");
    }

    public void customDestroyMethod() {
        logger.debug("BeanF -> customDestroyMethod()");
    }

    @Override
    public void validate() {
        logger.debug("BeanF -> validate()[interface MyBeanValidator]");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
