package com.epam.task1.pojo;

import com.epam.task1.MyBeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanB implements MyBeanValidator {

    private static Logger logger = LogManager.getLogger(BeanB.class);
    private String name;
    private int value;

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void customInitMethod() {
        logger.debug("BeanB -> customInitMethod()");
    }

    public void anotherCustomInitMethod() {
        logger.debug("BeanB -> anotherCustomInitMethod()[changed by MyBeanFactory]");
    }

    public void customDestroyMethod() {
        logger.debug("BeanB -> customDestroyMethod()");
    }

    @Override
    public void validate() {
        logger.debug("BeanB -> validate()[interface MyBeanValidator]");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
