package com.epam.task1;

import com.epam.task1.pojo.BeanA;
import com.epam.task1.pojo.BeanE;
import com.epam.task1.pojo.BeanF;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class BeanConfigTwo {

    @Bean(initMethod = "customInitMethod", destroyMethod = "customDestroyMethod")
    public BeanE getBeanE1(BeanA getBeanA1) {
        BeanE beanE = new BeanE();
        beanE.setName(getBeanA1.getName());
        beanE.setValue(getBeanA1.getValue());
        return beanE;
    }

    @Bean(initMethod = "customInitMethod", destroyMethod = "customDestroyMethod")
    public BeanE getBeanE2(BeanA getBeanA2) {
        BeanE beanE = new BeanE();
        beanE.setName(getBeanA2.getName());
        beanE.setValue(getBeanA2.getValue());
        return beanE;
    }

    @Bean(initMethod = "customInitMethod", destroyMethod = "customDestroyMethod")
    public BeanE getBeanE3(BeanA getBeanA3) {
        BeanE beanE = new BeanE();
        beanE.setName(getBeanA3.getName());
        beanE.setValue(getBeanA3.getValue());
        return beanE;
    }

    @Bean(initMethod = "customInitMethod", destroyMethod = "customDestroyMethod")
    @Lazy
    public BeanF getBeanF() {
        BeanF beanF = new BeanF();
        beanF.setName("beanFFF");
        return beanF;
    }
}
