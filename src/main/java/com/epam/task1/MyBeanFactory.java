package com.epam.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class MyBeanFactory implements BeanFactoryPostProcessor {

    private static Logger logger = LogManager.getLogger(MyBeanFactory.class);

    private void init() {
        logger.debug("MyBeanFactory -> init()");
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        logger.trace("---------- beans definitions ---------");
        for (String benName : beanFactory.getBeanDefinitionNames()) {
            BeanDefinition beanDefinition = beanFactory.getBeanDefinition(benName);
            if (benName.equals("getBeanB")) {
                beanDefinition.setInitMethodName("anotherCustomInitMethod");
            }
            logger.trace(beanDefinition);
        }
    }
}
