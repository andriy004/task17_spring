package com.epam.task1;

import com.epam.task1.pojo.BeanA;
import com.epam.task1.pojo.BeanB;
import com.epam.task1.pojo.BeanC;
import com.epam.task1.pojo.BeanD;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@Import(BeanConfigTwo.class) //can use an array of config classes here
@PropertySource("classpath:my.properties")
public class BeanConfigOne {

    @Value("${beanB.name}")
    private String nameB;
    @Value("${beanB.value}")
    private int valueB;
    @Value("${beanC.name}")
    private String nameC;
    @Value("${beanC.value}")
    private int valueC;
    @Value("${beanD.name}")
    private String nameD;
    @Value("${beanD.value}")
    private int valueD;

    @Bean(initMethod = "customInitMethod", destroyMethod = "customDestroyMethod")
    @DependsOn(value = {"getBeanD1", "getBeanD2", "getBeanB", "getBeanC1", "getBeanC2"})
    //injecting by type + injecting by name
    public BeanA getBeanA1(BeanB beanB, BeanC getBeanC1) {
        BeanA beanA = new BeanA();
        beanA.setName(beanB.getName());
        beanA.setValue(getBeanC1.getValue());
        return beanA;
    }

    @Bean(initMethod = "customInitMethod", destroyMethod = "customDestroyMethod")
    @DependsOn("getBeanA1")
    //injecting by type + injecting by qualifiers
    public BeanA getBeanA2(BeanB beanB, @Qualifier("myBeanD") BeanD beanD) {
        BeanA beanA = new BeanA();
        beanA.setName(beanB.getName());
        beanA.setValue(beanD.getValue());
        return beanA;
    }

    @Bean(initMethod = "customInitMethod", destroyMethod = "customDestroyMethod")
    @DependsOn("getBeanA2")
    //injecting by name to matching qualifier
    public BeanA getBeanA3(@Qualifier("getBeanC2") BeanC beanC, @Qualifier("getBeanD2") BeanD beanD) {
        BeanA beanA = new BeanA();
        beanA.setName(beanC.getName());
        beanA.setValue(beanD.getValue());
        return beanA;
    }

    @Bean(initMethod = "customInitMethod", destroyMethod = "customDestroyMethod")
    public BeanB getBeanB() {
        return new BeanB(nameB, valueB);
    }

    @Bean(initMethod = "customInitMethod", destroyMethod = "customDestroyMethod")
    public BeanC getBeanC1() {
        return new BeanC(nameC, valueC);
    }

    @Bean(initMethod = "customInitMethod", destroyMethod = "customDestroyMethod")
    public BeanC getBeanC2() {
        return new BeanC("CCC", 3000);
    }

    @Qualifier("myBeanD")
    @Bean(initMethod = "customInitMethod", destroyMethod = "customDestroyMethod")
    public BeanD getBeanD1() {
        return new BeanD(nameD, valueD);
    }

    @Bean(initMethod = "customInitMethod", destroyMethod = "customDestroyMethod")
    public BeanD getBeanD2() {
        return new BeanD("DDD", 4000);
    }

    @Bean
    public MyBeanPostProcessor getMyBean() {
        return new MyBeanPostProcessor();
    }

    @Bean(initMethod = "init")
    public MyBeanFactory getMyBeanFactory() {
        return new MyBeanFactory();
    }
}
