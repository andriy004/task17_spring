package com.epam.task2.beans4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanBossC {

    private OtherBeanC otherBeanC;

    @Autowired
    public void setOtherBeanC(OtherBeanC otherBeanC) {
        this.otherBeanC = otherBeanC;
    }

    @Override
    public String toString() {
        return "BeanBossC{" +
                "otherBeanC=" + otherBeanC +
                '}';
    }
}
