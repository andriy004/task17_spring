package com.epam.task2.beans4;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier("superBean")
@Scope("singleton")
public class OtherBeanA {

    @Override
    public String toString() {
        return "OtherBeanA{}";
    }
}
