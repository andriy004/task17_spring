package com.epam.task2.beans4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanBossB {

    private OtherBeanB otherBeanB;

    @Autowired
    public BeanBossB(OtherBeanB otherBeanB) {
        this.otherBeanB = otherBeanB;
    }

    @Override
    public String toString() {
        return "BeanBossB{" +
                "otherBeanB=" + otherBeanB +
                '}';
    }
}
