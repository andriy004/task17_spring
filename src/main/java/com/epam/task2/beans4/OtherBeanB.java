package com.epam.task2.beans4;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanB {

    @Override
    public String toString() {
        return "OtherBeanB{}";
    }
}
