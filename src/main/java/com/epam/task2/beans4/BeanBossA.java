package com.epam.task2.beans4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanBossA {

    @Autowired
    @Qualifier("superBean")
    private OtherBeanA otherBeanA;

    @Override
    public String toString() {
        return "BeanBossA{" +
                "otherBeanA=" + otherBeanA +
                '}';
    }
}
