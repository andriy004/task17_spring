package com.epam.task2.profileBeans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan
public class Config {

    @Bean
    @Profile("prod")
    public DataSource2 getProdDataSource() {
        String url = "//localhost:4105/prod_db";
        String user = "prod000user";
        String password = "prod000pass";
        return new DataSource2(url, user, password);
    }

    @Bean
    @Profile("dev")
    public DataSource2 getDevDataSource() {
        String url = "//localhost:4105/dev_db";
        String user = "dev000user";
        String password = "dev000pass";
        return new DataSource2(url, user, password);
    }
}
