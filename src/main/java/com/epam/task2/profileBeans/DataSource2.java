package com.epam.task2.profileBeans;

public class DataSource2 {

    private String url;
    private String user;
    private String password;

    public DataSource2(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    @Override
    public String toString() {
        return "DataSource2{" +
                "url='" + url + '\'' +
                ", user='" + user + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
