package com.epam.task2.profileBeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyDataBase {

    @Autowired
    DataSource3 dataSource3;

    @Override
    public String toString() {
        return dataSource3.toString();
    }
}
