package com.epam.task2.profileBeans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan
@Profile("dev")
public class ConfigDev {

    @Bean
    public DataSource getDataSource() {
        String url = "//localhost:4105/dev_db";
        String user = "dev000user";
        String password = "dev000pass";
        return new DataSource(url, user, password);
    }
}
