package com.epam.task2.profileBeans;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class DevelopmentDataSource extends DataSource3 {

    public DevelopmentDataSource() {
        url = "//localhost:4105/dev_db";
        user = "dev000user";
        password = "dev000pass";
    }

    @Override
    public String toString() {
        return "DevelopmentDataSource{" +
                "url='" + url + '\'' +
                ", user='" + user + '\'' +
                ", password='" + password + '\'' +
                "} ";
    }
}
