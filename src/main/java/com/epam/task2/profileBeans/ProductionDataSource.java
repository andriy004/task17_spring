package com.epam.task2.profileBeans;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("prod")
public class ProductionDataSource extends DataSource3 {

    public ProductionDataSource() {
        url = "//localhost:4105/prod_db";
        user = "prod000user";
        password = "prod000pass";
    }

    @Override
    public String toString() {
        return "ProductionDataSource{" +
                "url='" + url + '\'' +
                ", user='" + user + '\'' +
                ", password='" + password + '\'' +
                "} ";
    }
}
