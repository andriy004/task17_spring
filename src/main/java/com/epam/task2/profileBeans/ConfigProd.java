package com.epam.task2.profileBeans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan
@Profile("prod")
public class ConfigProd {

    @Bean
    public DataSource getDataSource() {
        String url = "//localhost:4105/prod_db";
        String user = "prod000user";
        String password = "prod000pass";
        return new DataSource(url, user, password);
    }
}
