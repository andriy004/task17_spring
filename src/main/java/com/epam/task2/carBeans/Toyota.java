package com.epam.task2.carBeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Primary
public class Toyota implements Car {

    @Override
    public String getCar() {
        return "Toyota";
    }
}
