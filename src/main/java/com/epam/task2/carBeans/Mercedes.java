package com.epam.task2.carBeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Qualifier("myCarMercedes")
public class Mercedes implements Car {

    @Override
    public String getCar() {
        return "Mercedes";
    }
}
