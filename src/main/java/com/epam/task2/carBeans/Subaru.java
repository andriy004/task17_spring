package com.epam.task2.carBeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
@Qualifier("myCarSubaru")
public class Subaru implements Car {

    @Override
    public String getCar() {
        return "Subaru";
    }
}
