package com.epam.task2.carBeans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GeneralBeanOne {

    private static Logger logger = LogManager.getLogger(GeneralBeanOne.class);

    @Autowired
    private List<Car> cars;

    public void printCars() {
        for (Car car : cars) {
            logger.info(car.getCar());
        }
    }
}
