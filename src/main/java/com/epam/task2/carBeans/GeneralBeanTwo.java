package com.epam.task2.carBeans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class GeneralBeanTwo {

    private static Logger logger = LogManager.getLogger(GeneralBeanTwo.class);

    @Autowired
    private Car car1;

    @Autowired
    @Qualifier("myCarRenault")
    private Car car2;

    @Autowired
    @Qualifier("myCarSubaru")
    private Car car3;

    @Autowired
    @Qualifier("myCarMercedes")
    private Car car4;

    public void printCars() {
        logger.info("car1 = " + car1.getCar()
                + "\ncar2 = " + car2.getCar()
                + "\ncar3 = " + car3.getCar()
                + "\ncar4 = " + car4.getCar());
    }
}
