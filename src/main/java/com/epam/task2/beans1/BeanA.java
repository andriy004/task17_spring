package com.epam.task2.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanA {

    @Override
    public String toString() {
        return "BeanA{}";
    }
}
