package com.epam.task2;

import com.epam.task2.beans2.CatAnimal;
import com.epam.task2.beans3.BeanD;
import com.epam.task2.beans3.BeanF;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "com.epam.task2.beans2",
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = CatAnimal.class))
@ComponentScan(basePackages = "com.epam.task2.beans3",
        includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {BeanD.class, BeanF.class}))
public class BeanConfigTwo {

}
