package com.epam.task2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.epam.task2.beans1")
public class BeanConfigOne {

}
