package com.epam;

import com.epam.task1.BeanConfigOne;
import com.epam.task1.pojo.*;
import com.epam.task2.carBeans.BeanConfigCar;
import com.epam.task2.BeanConfigThree;
import com.epam.task2.BeanConfigTwo;
import com.epam.task2.beans4.OtherBeanA;
import com.epam.task2.beans4.OtherBeanB;
import com.epam.task2.beans4.OtherBeanC;
import com.epam.task2.carBeans.GeneralBeanOne;
import com.epam.task2.carBeans.GeneralBeanTwo;
import com.epam.task2.profileBeans.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

public class Application {

    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        logger.fatal("======================= TASK 1 =======================");
        printTask1();
        logger.fatal("\n\n======================= TASK 2 =======================");
        printTask2();
    }

    private static void printTask1() {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(BeanConfigOne.class);
        logger.info("-----------------------");
        Map<String, BeanA> beanListA = context.getBeansOfType(BeanA.class);
        logger.info("beanListA: " + beanListA);
        BeanB beanB = context.getBean(BeanB.class);
        logger.info(beanB);
        Map<String, BeanC> beanListC = context.getBeansOfType(BeanC.class);
        logger.info("beanListC: " + beanListC);
        Map<String, BeanD> beanListD = context.getBeansOfType(BeanD.class);
        logger.info("beanListD: " + beanListD);
        Map<String, BeanE> beanListE = context.getBeansOfType(BeanE.class);
        logger.info("beanListE: " + beanListE);
        BeanF beanF = context.getBean(BeanF.class);
        logger.info(beanF);
        logger.info("-----------------------");
        ((AnnotationConfigApplicationContext) context).close();
    }

    private static void printTask2() {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(com.epam.task2.BeanConfigOne.class);
        logger.debug("------------- @ComponentScan");
        for (String beanName : context.getBeanDefinitionNames())
            logger.info(beanName + " = " + context.getBean(beanName).toString());

        ApplicationContext context2 =
                new AnnotationConfigApplicationContext(BeanConfigTwo.class);
        logger.debug("------------- @ComponentScan with includeFilters and excludeFilters");
        for (String beanName : context2.getBeanDefinitionNames())
            logger.info(beanName + " = " + context2.getBean(beanName).toString());

        ApplicationContext context3 =
                new AnnotationConfigApplicationContext(BeanConfigThree.class);
        logger.debug("------------- @Autowire -> constructor, setter, field");
        for (String beanName : context3.getBeanDefinitionNames())
            logger.info(beanName + " = " + context3.getBean(beanName).toString());
        logger.debug("------------- @Scope, @Prototype");
        logger.info("BeanA: " + context3.getBean(OtherBeanA.class).hashCode());
        logger.info("BeanA: " + context3.getBean(OtherBeanA.class).hashCode());
        logger.info("BeanB: " + context3.getBean(OtherBeanB.class).hashCode());
        logger.info("BeanB: " + context3.getBean(OtherBeanB.class).hashCode());
        logger.info("BeanC: " + context3.getBean(OtherBeanC.class).hashCode());
        logger.info("BeanC: " + context3.getBean(OtherBeanC.class).hashCode());

        ApplicationContext context4 =
                new AnnotationConfigApplicationContext(BeanConfigCar.class);
        logger.debug("------------- @Order");
        context4.getBean(GeneralBeanOne.class).printCars();
        logger.debug("------------- @Primary, @Qualifier");
        context4.getBean(GeneralBeanTwo.class).printCars();

        AnnotationConfigApplicationContext context5 = new AnnotationConfigApplicationContext();
        context5.getEnvironment().setActiveProfiles("dev");
        context5.register(ConfigDev.class);
        context5.refresh();
        logger.debug("------------- @Configuration + @Profile");
        logger.info(context5.getBean(DataSource.class));

        AnnotationConfigApplicationContext context6 = new AnnotationConfigApplicationContext();
        context6.getEnvironment().setActiveProfiles("prod");
        context6.register(Config.class);
        context6.refresh();
        logger.debug("------------- @Bean + @Profile");
        logger.info(context6.getBean(DataSource2.class));
        logger.debug("------------- @Component + @Profile");
        logger.info(context6.getBean(MyDataBase.class));
    }
}
